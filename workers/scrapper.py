""" Scrapper

Worker to access real time matches and scores
"""

import logging
import sports
import os
import time
import requests
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
envdiv = os.path.join(basedir, '..')
load_dotenv(os.path.join(envdiv, '.env'))

CACHE = []
MATCHES = []


def _get_sport(sport_name):
    return {
        'FOOTBALL': sports.FOOTBALL,
        'BASKETBALL': sports.BASKETBALL,
        'SOCCER': sports.SOCCER
        }.get(sport_name, sports.SOCCER)


def format_football_matches(matches):
    """Format an array of Match to an array of dict

    Args:
        matches (array): Array of Match

    Returns:
        matches_formatted: An array of dict. An example of a dict:
            {'home_team': 'A Team',
            'away_team': 'B Team',
            'home_score': 0,
            'away_score': 0})
    """
    if not matches:
        logging.warning('Matches list is empty')
        return []
    try:
        matches_formatted = []
        for match in matches:
            matches_formatted.append(
                {'home_team': match.home_team, 'away_team': match.away_team, 'home_score': match.home_score,
                 'away_score': match.away_score})
        return matches_formatted
    except Exception:
        logging.exception('Error formatting matches')
        return []


def get_matches(sport):
    """Get all the FOOTBALL (not SOCCER) matches

            Returns:
                matches: An array of dict. An example of a dict:
                    {'home_team': 'A Team',
                    'away_team': 'B Team',
                    'home_score': 0,
                    'away_score': 0})
    """
    try:
        matches = sports.get_sport(sport)
    except Exception:
        logging.exception('Error scrapping sports')
        return []
    logging.info('Football matches scrapped!')
    if not matches:
        logging.warning('No matches')
        return []
    return matches


def update_matches(url, header, matches):
    try:
        logging.info(url)
        response = requests.post(url, headers=header, json=matches)
        logging.info(response.status_code)
        if response.status_code != 200:
            logging.error('Response is not 200: {response}'.format(response=response.status_code))
    except Exception:
        logging.exception('Server is down!')


def get_token_from_server(url):
    logging.info('Getting JWT for Scrapper')
    user = os.environ.get('SCRAPPER_USER') or 'scrapper'
    password = os.environ.get('SCRAPPER_PASS') or 'scrapper1234'
    logging.info('Username: {}, Password:{}'.format(user, password))
    try:
        response = requests.post(url, json={
            'username': user,
            'password': password
            })
        if response.status_code != 200:
            logging.error('Response is not 200: {response}'.format(response=response.json()))
            return ''
        body = response.json()
        return body['access_token']
    except Exception:
        logging.exception('Server is down!')
        return ''


if __name__ == '__main__':
    url_login = os.getenv('LOGIN_URL') or 'http://localhost:5000/api/login'
    url_matches = os.getenv('MATCHES_URL') or 'http://localhost:5000/api/matches'

    logging.getLogger().setLevel(logging.INFO)
    SPORT = _get_sport(os.getenv('SPORT'))

    auth_token = get_token_from_server(url_login)
    if not auth_token:
        logging.debug('Scrapper didnt get a token')
        exit(-1)

    while True:
        MATCHES = get_matches(SPORT.upper())
        matches_formatted = format_football_matches(MATCHES)
        if matches_formatted:
            update_matches(url_matches, {'Authorization': 'Bearer ' + auth_token}, matches_formatted)
            CACHE = matches_formatted
        logging.info('Time to sleep!')
        time.sleep(10)
