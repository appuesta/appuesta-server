""" APPuesta Server

If a client wants to get all the live matches and scores will use this server.

This server uses sockets to transmit info about the live matches.
"""
from app import create_app, socketio, cli

app = create_app(debug=True)
cli.register(app)

if __name__ == '__main__':
    socketio.run(app)
