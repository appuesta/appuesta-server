from app import db
from app.models import Match
from sqlalchemy import Date, cast, func
from datetime import date
import logging


class MatchesSingleton:
    def __init__(self):
        self.list_matches = []

    @staticmethod
    def _serialize(match):
        return {
            'home_team': match.home_team,
            'away_team': match.away_team,
            'home_score': match.home_score,
            'away_score': match.away_score,
            'id': match.id,
            'sport': match.sport
            }

    @staticmethod
    def is_valid_match(match):
        return 'home_team' in match and \
               'away_team' in match and \
               'home_score' in match and \
               'away_score' in match and \
               'sport' in match

    def all_matches(self):
        if not self.list_matches:
            # matches_db = db.session.query(Match).filter(cast(Match.date, Date) == date.today()).all()
            matches_db = db.session.query(Match).filter(func.DATE(Match.date) == date.today())

            if not matches_db:
                logging.info('There are not matches for today')
                return []
            matches = []
            for match in matches_db:
                matches.append(MatchesSingleton._serialize(match))
            self.list_matches = matches
        return self.list_matches

    def add_match(self, match):
        if not self.list_matches:
            self.all_matches()
        for item in self.list_matches:
            if item['home_team'] == match['home_team'] and item['away_team'] == match['away_team']:
                if item['home_score'] != match['home_score'] or item['away_score'] != match['away_score']:
                    logging.info('Updating score of the match in database')
                    db.session.query(Match).filter(Match.id == item['id']).update(
                        {'home_score': match['home_score'], 'away_score': match['away_score']},
                        synchronize_session='fetch')
                    db.session.commit()
                    item['home_score'] = match['home_score']
                    item['away_score'] = match['away_score']
                else:
                    logging.info('Same score')
                break
        return self.list_matches
