from app.models import User
import logging


def is_valid_user(username, password):
    user = User.query.filter_by(username=username).first()
    if user is None:
        logging.info('User doesnt exist in database')
        return False
    return user.check_password(password)


def get_token_for_user(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        logging.info('User doesnt exist in database')
        return None
    return user.get_token()


def revoke_token_for_user(username):
    user = User.query.filter_by(username=username).first()
    if user:
        user.revoke_token()
