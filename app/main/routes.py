from flask import render_template
from app.main import bp


@bp.route('/')
def main_page():
    """
    Just for testing. If you open the url into a browser, you have the same socket than you will have for an app so
    you can debug easily.
    """
    return render_template('index.html')
