from appuesta import socketio
from flask import jsonify, request, current_app
from flask_socketio import Namespace
from app.helpers.matches_singleton import MatchesSingleton


class MatchesSocket(Namespace):
    list_matches = MatchesSingleton()

    def on_connect(self):
        current_app.logger.info('Connected')
        current_app.logger.info('Matches: {}'.format(self.list_matches.all_matches()))
        socketio.emit('new_matches', self.list_matches.all_matches(), namespace='/socket')

    def on_disconnect(self):
        pass

    def on_get_matches(self, data):
        current_app.logger.info('get matches')
        current_app.logger.info('Matches: {}'.format(self.list_matches.all_matches()))
        socketio.emit('new_matches', self.list_matches.all_matches(), namespace='/socket')
