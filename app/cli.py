# pylint: disable=W0612

import os
import datetime as dt
from numpy import genfromtxt
from werkzeug.security import generate_password_hash

from app import db, migrate
from .models import Match, User


def register(application):
    def _get_data(file):
        data = genfromtxt(file, delimiter=';', names=True, dtype=None, encoding='utf-8')
        return data.tolist()

    def _fit_date(date):
        return dt.datetime.strptime(date, '%d/%m/%Y %H:%M')

    def _add_seed(seed_file, sport):
        """Populate database with NBA games using seed_file csv"""
        try:
            matches = _get_data(seed_file)
            for match in matches:
                record = Match(**{
                    'date': _fit_date(match[0]),
                    'home_team': match[1],
                    'away_team': match[2],
                    'home_score': 0,
                    'away_score': 0,
                    'sport': sport
                    })
                db.session.add(record)
            db.session.commit()
            application.logger.debug('Commit {}!'.format(sport))
        except Exception:
            db.session.rollback()
            application.logger.exception('Exception!')
        finally:
            db.session.close()

    def _create_basic_users():
        try:
            basic_user = User(**{
                'username': os.environ.get('BASIC_USER_NAME'),
                'email': os.environ.get('BASIC_USER_MAIL'),
                'password_hash': generate_password_hash(os.environ.get('BASIC_USER_PASS'))
                })
            db.session.add(basic_user)
            scrapper_user = User(**{
                'username': os.environ.get('SCRAPPER_USER'),
                'email': os.environ.get('SCRAPPER_MAIL'),
                'password_hash': generate_password_hash(os.environ.get('SCRAPPER_PASS'))
                })
            db.session.add(scrapper_user)
            db.session.commit()
            application.logger.debug('Commit Users!')
        except Exception:
            db.session.rollback()
            application.logger.exception('Exception!')
        finally:
            db.session.close()

    @application.cli.group()
    def seed():
        """Seed basic database"""
        migrate.init_app(application, db)

    @seed.command()
    def start():
        db_folder = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'db'))
        application.logger.info('Creating users...')
        _create_basic_users()
        application.logger.info('Inserting all NBA matches...')
        _add_seed(os.path.join(db_folder, 'nba-matches.csv'), 'basketball')
        application.logger.info('Inserting all NFL matches...')
        _add_seed(os.path.join(db_folder, 'nfl-matches.csv'), 'football')
        application.logger.info('Done!')

    @seed.command()
    def delete_all():
        """Delete all the rows in the database"""
        try:
            num_rows = db.session.query(Match).delete()
            application.logger.info('{} rows deleted'.format(num_rows))
            db.session.commit()
        except Exception:
            application.logger.exception('Error deleting database')
            db.session.rollback()
