from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_socketio import SocketIO
from flask_jwt_extended import JWTManager
from config import Config

db = SQLAlchemy()
migrate = Migrate()
socketio = SocketIO()
jwt = JWTManager()


def create_app(debug=False, config_class=Config):
    app = Flask(__name__)
    app.debug = debug
    app.config.from_object(config_class)
    jwt.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)

    from app.main import bp as main_bp
    app.register_blueprint(main_bp)
    from app.api import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    socketio.init_app(app, cors_allowed_origins=["http://localhost", "http://localhost:5000"])

    from app.socket.matches_socket import MatchesSocket
    socketio.on_namespace(MatchesSocket('/socket'))

    return app
