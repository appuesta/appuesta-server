from flask import jsonify, request, current_app
from app import db
from app.models import Match, Bet, User
from app.api import bp
from app.api.errors import bad_request
from flask_jwt_extended import jwt_required, get_jwt_identity


@bp.route('/bets', methods=['POST'])
@jwt_required
def new_bet():
    current_user = get_jwt_identity()
    current_app.logger.info('New bet from user: {user}'.format(user=current_user))

    match_id = request.json.get('match_id', None)
    result = request.json.get('result', None)
    current_app.logger.info('Match: {match} : {result}'.format(match=match_id, result=result))
    if not match_id:
        return bad_request("Missing a valid match_id")
    if not result:
        return bad_request("Missing a valid result")
    if result not in['W', 'D', 'L']:
        return bad_request("Invalid result")

    try:
        user = db.session.query(User).filter(User.username == current_user).first()
        if user is None:
            return bad_request("Invalid user")
    except Exception:
        current_app.logger.exception('Something was wrong')
        return bad_request("Something was wrong")

    try:
        match = db.session.query(Match).filter(Match.id == match_id).first()
        if match is None:
            return bad_request("Invalid match")
    except Exception:
        current_app.logger.exception('Something was wrong')
        return bad_request("Something was wrong")

    try:
        if db.session.query(Bet).filter(Bet.user_id == user.id, Bet.match_id == match.id).first() is None:
            bet = Bet(user_id=user.id, match_id=match.id, result=result)
            db.session.add(bet)
            db.session.commit()
        else:
            return bad_request("The bet already exists")
    except Exception:
        current_app.logger.exception('Something was wrong')
        return bad_request("Something was wrong")
    return jsonify({}), 200
