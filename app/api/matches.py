from flask import jsonify, request, current_app
from app.api import bp
from app.api.errors import bad_request
from appuesta import socketio
from app.helpers.matches_singleton import MatchesSingleton
from flask_jwt_extended import jwt_required

list_matches = MatchesSingleton()


@bp.route('/matches', methods=['POST'])
@jwt_required
def new_matches():
    current_app.logger.info('New matches from a scrapper')
    data = request.get_json() or {}
    if not data or data == [] or not isinstance(data, list):
        current_app.logger.debug('Invalid data: {data}'.format(data=data))
        return bad_request('Data is invalid.')
    for match in data:
        current_app.logger.debug(match)
        if list_matches.is_valid_match(match):
            list_matches.add_match(match)
        else:
            current_app.logger.warning('Match is not valid')
    current_app.logger.info('sending matches for the socket')
    socketio.emit('matches', list_matches.all_matches(), broadcast=True)
    response = jsonify({})
    response.status_code = 200
    return response
