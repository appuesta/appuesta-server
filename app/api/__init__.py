from flask import Blueprint

bp = Blueprint('api', __name__)

from app.api import matches
from app.api import bets
from app.api import auth
