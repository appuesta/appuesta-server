from flask import jsonify, request, current_app
from app.api import bp
from app.api.errors import bad_request, unauthorize
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity
from app.helpers.login_helper import is_valid_user, get_token_for_user, revoke_token_for_user


@bp.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return bad_request("Missing JSON in request")

    username = request.json.get('username', None)
    password = request.json.get('password', None)

    if not username:
        return bad_request("Missing username parameter")
    if not password:
        return bad_request("Missing password parameter")

    if not is_valid_user(username, password):
        return unauthorize("Bad username or password")

    access_token = get_token_for_user(username)
    if not access_token:
        return unauthorize("Bad token request")

    return jsonify(access_token=access_token), 200


@bp.route('/logout', methods=['POST'])
@jwt_required
def logout():
    username = get_jwt_identity()
    revoke_token_for_user(username)

    return jsonify({}), 200
