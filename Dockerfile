FROM python:3.6

ARG URL
ARG SPORT

ENV URL=$URL
ENV SPORT=$SPORT

WORKDIR /code
RUN pip install pipenv
COPY . .
RUN pipenv lock --requirements > requirements.txt
RUN pip install -r requirements.txt
CMD gunicorn appuesta:app --worker-class eventlet -w 1 --bind 0.0.0.0:5000 --reload