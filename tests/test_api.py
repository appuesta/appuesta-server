import json
from app.helpers.matches_singleton import MatchesSingleton
from .factories.match_factory import MatchFactory
from flask_jwt_extended import create_access_token
from .factories.user_factory import UserFactory
from app.models import Bet, Match
import logging


def mock_access_token(user):
    access_token = user.get_token()
    return {
        'Authorization': 'Bearer {}'.format(access_token)
        }


def test_updating_without_token(app, session):
    MatchFactory.create()

    with app.test_client() as c:
        values = [{'home_team': 'A', 'away_team': 'B', 'home_score': 1, 'away_score': 0, 'sport': 'basketball'}]
        resp = c.post('api/matches', data=json.dumps(values), content_type='application/json')
        assert resp.status_code == 401


def test_new_matches_update_score_in_list(app, session):
    md = MatchesSingleton()
    user = UserFactory.create()
    MatchFactory.create()

    with app.test_client() as c:
        values = [{'home_team': 'A', 'away_team': 'B', 'home_score': 1, 'away_score': 0, 'sport': 'basketball'}]
        resp = c.post('api/matches', headers=mock_access_token(user), data=json.dumps(values),
                      content_type='application/json')
        assert resp.status_code == 200
        assert md.all_matches() == [{'away_score': 0,
                                     'away_team': 'B',
                                     'home_score': 1,
                                     'home_team': 'A',
                                     'id': 0,
                                     'sport': 'basketball'}]


def test_new_matches_with_empty_database_dont_update_list(app, session):
    user = UserFactory.create()

    with app.test_client() as c:
        values = [{'home_team': 'A', 'away_team': 'A', 'home_score': 1, 'away_score': 0, 'sport': 'basketball'}]
        resp = c.post('api/matches', headers=mock_access_token(user), data=json.dumps(values),
                      content_type='application/json')
        assert resp.status_code == 200
        md = MatchesSingleton()
        assert md.all_matches() == []


def test_new_matches_dont_update_list(app, session):
    md = MatchesSingleton()
    user = UserFactory.create()
    MatchFactory.create()

    with app.test_client() as c:
        values = [{'home_team': 'A', 'away_team': 'C', 'home_score': 1, 'away_score': 0, 'sport': 'basketball'}]
        resp = c.post('api/matches',
                      headers=mock_access_token(user),
                      data=json.dumps(values),
                      content_type='application/json')
        assert resp.status_code == 200
        assert md.all_matches() == [{'away_score': 0,
                                     'away_team': 'B',
                                     'home_score': 0,
                                     'home_team': 'A',
                                     'id': 0,
                                     'sport': 'basketball'}]


def test_new_matches_empty(app, session):
    user = UserFactory.create()

    with app.test_client() as c:
        resp = c.post('api/matches', headers=mock_access_token(user), data={})
        data = json.loads(resp.data.decode())
        assert resp.status_code == 400
        assert 'Data is invalid.' in data['message']
