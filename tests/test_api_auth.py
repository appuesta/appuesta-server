from .factories.user_factory import UserFactory
import json


def test_auth_as_user(app, session):
    user = UserFactory()
    with app.test_client() as c:
        resp = c.post('api/login', json={'username': user.username, 'password': '1234'})
        data = json.loads(resp.data.decode())
        assert resp.status_code == 200
        assert data['access_token'] is not None


def test_auth_with_invalid_pass(app, session):
    user = UserFactory()
    with app.test_client() as c:
        resp = c.post('api/login', json={'username': user.username, 'password': '123456'})
        assert resp.status_code == 401


def test_auth_with_invalid_data(app, session):
    user = UserFactory()
    with app.test_client() as c:
        resp = c.post('api/login', json={'username': user.username})
        assert resp.status_code == 400
        resp = c.post('api/login', json={'password': '123456'})
        assert resp.status_code == 400
