import pytest
from workers.scrapper import format_football_matches, get_matches


def test_format_football_matches(mocker):
    match = mocker.Mock()
    match.home_team = 'A Team'
    match.away_team = 'B Team'
    match.home_score = 0
    match.away_score = 0
    assert format_football_matches([match]) == [
        {'home_team': 'A Team', 'away_team': 'B Team', 'home_score': 0,
         'away_score': 0}]


def test_format_football_matches_empty():
    assert format_football_matches(None) == []


def test_format_football_matches_string():
    assert format_football_matches('a') == []


def test_format_football_matches_empty_array():
    assert format_football_matches([]) == []


def test_format_football_matches_invalid_data():
    assert format_football_matches([{'a': 'a'}]) == []


def test_get_matches(mocker):
    match = mocker.Mock()
    match.home_team = 'A Team'
    match.away_team = 'B Team'
    match.home_score = 0
    match.away_score = 0

    matches = mocker.patch("sports.get_sport")
    matches.return_value = [match]

    assert format_football_matches(get_matches("FOOTBALL")) == [
        {'home_team': 'A Team', 'away_team': 'B Team', 'home_score': 0,
         'away_score': 0}]


def test_get_matches_exception(mocker):
    matches = mocker.patch("sports.get_sport")
    matches.side_effect = TimeoutError
    assert get_matches("FOOTBALL") == []


def test_football_matches_no_matches(mocker):
    matches = mocker.patch("sports.get_sport")
    matches.return_value = []
    assert get_matches("FOOTBALL") == []
