import pytest
from app import create_app
from app import db as _db
from .factories.match_factory import MatchFactory
from .factories.user_factory import UserFactory
from .factories.bet_factory import BetFactory


class TestConfig(object):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    REDIS_URL = None
    SECRET_KEY = 'secret'


@pytest.fixture(scope="session")
def app(request):
    app = create_app(debug=False, config_class=TestConfig)
    return app


@pytest.fixture(autouse=True)
def _setup_app_context_for_test(request, app):
    """
    Given app is session-wide, sets up a app context per test to ensure that
    app and request stack is not shared between tests.
    """
    ctx = app.app_context()
    ctx.push()
    yield  # tests will run here
    ctx.pop()


@pytest.fixture(scope="session")
def db(app, request):
    """Returns session-wide initialized database"""
    with app.app_context():
        _db.create_all()
        yield _db
        _db.drop_all()


@pytest.fixture(scope="function")
def session(app, db, request):
    """Creates a new database session for each test, rolling back changes afterwards"""
    connection = _db.engine.connect()
    transaction = connection.begin()

    options = dict(bind=connection, binds={})
    session = _db.create_scoped_session(options=options)

    _db.session = session
    MatchFactory._meta.sqlalchemy_session = session
    UserFactory._meta.sqlalchemy_session = session
    BetFactory._meta.sqlalchemy_session = session

    yield session

    transaction.rollback()
    connection.close()
    session.remove()
