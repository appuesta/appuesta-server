from .factories.match_factory import MatchFactory
import datetime
from app.helpers.matches_singleton import MatchesSingleton


def test_list_is_empty_if_database_is_empty(session):
    list_matches = MatchesSingleton()
    assert list_matches.all_matches() == []
    # Create a match for tomorrow and check it doesn't appears in the list
    MatchFactory.create(date=datetime.date.today() + datetime.timedelta(days=1))
    assert list_matches.all_matches() == []


def test_list_returns_matches_for_the_day(session):
    list_matches = MatchesSingleton()
    # Match for today
    MatchFactory.create(id=2)

    # Match for tomorrow
    MatchFactory.create(id=1, home_team='C', away_team='D', date=datetime.date.today() + datetime.timedelta(days=1))

    assert list_matches.all_matches() == [{'away_score': 0,
                                           'away_team': 'B',
                                           'home_score': 0,
                                           'home_team': 'A',
                                           'id': 2,
                                           'sport': 'basketball'}]


def test_update_a_valid_match(session):
    list_matches = MatchesSingleton()
    MatchFactory.create(id=4)
    list_matches.add_match({'away_score': 0,
                            'away_team': 'B',
                            'home_score': 1,
                            'home_team': 'A',
                            'sport': 'basketball'})
    assert list_matches.all_matches() == [{'away_score': 0,
                                           'away_team': 'B',
                                           'home_score': 1,
                                           'home_team': 'A',
                                           'id': 4,
                                           'sport': 'basketball'}]


def test_update_an_invalid_match(session):
    list_matches = MatchesSingleton()
    assert list_matches.all_matches() == []
    list_matches.add_match({'away_score': 0,
                            'away_team': 'C',
                            'home_score': 1,
                            'home_team': 'A',
                            'sport': 'basketball'})
    assert list_matches.all_matches() == []

    MatchFactory.create()
    list_matches.add_match({'away_score': 0,
                            'away_team': 'C',
                            'home_score': 1,
                            'home_team': 'A',
                            'sport': 'basketball'})
    assert list_matches.all_matches() == [{'away_score': 0,
                                           'away_team': 'B',
                                           'home_score': 0,
                                           'home_team': 'A',
                                           'id': 0,
                                           'sport': 'basketball'}]
