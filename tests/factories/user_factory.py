import factory
from app.models import User
from datetime import datetime, timedelta
from werkzeug.security import generate_password_hash


class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
    id = 0
    username = 'user'
    email = 'test@test.com'
    password_hash = generate_password_hash('1234')
    token = None
    token_expiration = datetime.utcnow() + timedelta(seconds=3600)

    class Meta:
        model = User
