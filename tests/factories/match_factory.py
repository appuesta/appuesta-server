import factory
from app.models import Match
from datetime import datetime


class MatchFactory(factory.alchemy.SQLAlchemyModelFactory):
    id = 0
    home_team = 'A'
    away_team = 'B'
    home_score = 0
    away_score = 0
    date = datetime.utcnow()
    sport = 'basketball'

    class Meta:
        model = Match
