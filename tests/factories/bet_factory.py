import factory
from app.models import Bet
from .user_factory import UserFactory
from .match_factory import MatchFactory


class BetFactory(factory.alchemy.SQLAlchemyModelFactory):
    id = 0
    match_id = factory.SubFactory(MatchFactory)
    user_id = factory.SubFactory(UserFactory)
    result = 'W'

    class Meta:
        model = Bet
