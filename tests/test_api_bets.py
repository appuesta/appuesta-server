from .factories.match_factory import MatchFactory
from .factories.user_factory import UserFactory
from app.models import Bet


def mock_access_token(user):
    access_token = user.get_token()
    return {
        'Authorization': 'Bearer {}'.format(access_token)
        }


def test_new_bet(app, session):
    user = UserFactory.create(id=1)
    match = MatchFactory.create(id=1)
    with app.test_client() as c:
        resp = c.post('api/bets', headers=mock_access_token(user), json={'match_id': match.id, 'result': 'W'})
        assert resp.status_code == 200
        assert Bet.query.filter_by(user_id=user.id, match_id=match.id).first() is not None


def test_new_bet_with_invalid_data(app, session):
    user = UserFactory.create(id=1)
    match = MatchFactory.create(id=1)
    with app.test_client() as c:
        resp = c.post('api/bets', headers=mock_access_token(user), json={})
        assert resp.status_code == 400

        resp = c.post('api/bets', headers=mock_access_token(user), json={'match_id': match.id})
        assert resp.status_code == 400

        resp = c.post('api/bets', headers=mock_access_token(user), json={'match_id': match.id, 'result': 'E'})
        assert resp.status_code == 400


def test_new_bet_with_invalid_match_id(app, session):
    user = UserFactory.create(id=1)
    with app.test_client() as c:
        resp = c.post('api/bets', headers=mock_access_token(user), json={'match_id': 'invalid', 'result': 'W'})
        assert resp.status_code == 400


def test_new_bet_with_a_bet_done(app, session):
    user = UserFactory.create(id=1)
    match = MatchFactory.create(id=1)
    with app.test_client() as c:
        resp = c.post('api/bets', headers=mock_access_token(user), json={'match_id': match.id, 'result': 'W'})
        assert resp.status_code == 200
        resp = c.post('api/bets', headers=mock_access_token(user), json={'match_id': match.id, 'result': 'W'})
        assert resp.status_code == 400
