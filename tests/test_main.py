import json
from flask import url_for


def test_index(app):
    client = app.test_client()
    assert client.get('/').status_code == 200
