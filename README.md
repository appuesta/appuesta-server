[![pipeline status](https://gitlab.com/appuesta/appuesta-server/badges/master/pipeline.svg)](https://gitlab.com/appuesta/appuesta-server/commits/master)
[![coverage report](https://gitlab.com/appuesta/appuesta-server/badges/master/coverage.svg)](https://gitlab.com/appuesta/appuesta-server/commits/master)

# APPuesta Server

A Flask server that allows different clients to connect through SocketIO and receive match information and results in real time


## Setup

You need to have [pipenv](https://github.com/pypa/pipenv) installed in your computer.

Once you have installed `pipenv` you only have to install all the dependencies.

```bash
$ pipenv install --dev      # For development 
$ pipenv install            # For running
```

To have some previous matches, the repo contains two different scripts to populate the database:

```bash
$ pipenv run flask seed start       # Insert in the database NBA and NFL matches
$ pipenv run flask seed delete-all  # Remove all the matches in the database
```

## Execution

**Don't use flask** there are an issue related to the websocket in this current version of Flask.

If you want to use it:

```bash
$ pipenv run python app.py
```

To execute all the environment just use `docker-compose`:
```bash
$ docker-compose up --build -d
```

## Testing

If you want to use it and test what is sending the socket you can do it simply opening `localhost:5000`. In this URL you will see a pretty simple page with a basic javascript how shows the same message a client will see.

To run all the tests you need `py-tests` and you can do it using:

```bash
$ pipenv run pytest tests
```
